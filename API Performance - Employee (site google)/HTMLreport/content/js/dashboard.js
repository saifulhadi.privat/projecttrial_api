/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 30.76923076923077, "KoPercent": 69.23076923076923};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.21153846153846154, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "DELETE Employees"], "isController": false}, {"data": [0.75, 500, 1500, "POST Register"], "isController": false}, {"data": [0.0, 500, 1500, "GET Dashboard"], "isController": false}, {"data": [0.0, 500, 1500, "GET Employees"], "isController": false}, {"data": [0.0, 500, 1500, "GET All Grades by Level"], "isController": false}, {"data": [1.0, 500, 1500, "GET Detail Employees"], "isController": false}, {"data": [0.0, 500, 1500, "POST Create Employees"], "isController": false}, {"data": [0.0, 500, 1500, "POST Forgot Password"], "isController": false}, {"data": [0.0, 500, 1500, "POST Login"], "isController": false}, {"data": [0.0, 500, 1500, "POST Change Password"], "isController": false}, {"data": [0.0, 500, 1500, "GET Dashboard Direktur"], "isController": false}, {"data": [0.0, 500, 1500, "PUT Update Employees"], "isController": false}, {"data": [1.0, 500, 1500, "GET All Grades"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 130, 90, 69.23076923076923, 419.6461538461537, 246, 2006, 264.0, 1128.5, 1713.6, 1975.3099999999997, 8.031136096867858, 67.55517544943473, 2.0825932847346635], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["DELETE Employees", 10, 10, 100.0, 261.59999999999997, 246, 281, 262.0, 280.1, 281.0, 281.0, 0.9492168960607499, 0.7323060037968676, 0.1816860465116279], "isController": false}, {"data": ["POST Register", 10, 0, 0.0, 742.1, 341, 1163, 740.5, 1159.9, 1163.0, 1163.0, 0.8535336292249915, 0.3900915414817344, 0.21338340730624789], "isController": false}, {"data": ["GET Dashboard", 10, 10, 100.0, 264.20000000000005, 251, 286, 261.5, 285.0, 286.0, 286.0, 0.9250693802035153, 0.5122210337650324, 0.16983695652173914], "isController": false}, {"data": ["GET Employees", 10, 0, 0.0, 1800.6000000000001, 1665, 2006, 1768.0, 1996.1000000000001, 2006.0, 2006.0, 0.8194034742707309, 82.78375686250409, 0.14003477343493936], "isController": false}, {"data": ["GET All Grades by Level", 10, 10, 100.0, 263.6, 255, 279, 263.0, 278.1, 279.0, 279.0, 0.9264406151565684, 0.5410268436168242, 0.17099343385214008], "isController": false}, {"data": ["GET Detail Employees", 10, 0, 0.0, 262.99999999999994, 252, 276, 262.5, 275.9, 276.0, 276.0, 0.946880030300161, 1.1114744105671812, 0.16366969273743018], "isController": false}, {"data": ["POST Create Employees", 10, 10, 100.0, 278.49999999999994, 264, 292, 276.5, 291.4, 292.0, 292.0, 0.9450902561194594, 0.6820524406955865, 0.5122315352991211], "isController": false}, {"data": ["POST Forgot Password", 10, 10, 100.0, 263.7, 250, 291, 262.5, 289.2, 291.0, 291.0, 0.9237875288683602, 0.6323975173210162, 0.2101977482678984], "isController": false}, {"data": ["POST Login", 10, 10, 100.0, 263.3, 248, 282, 264.0, 280.9, 282.0, 282.0, 0.9231905465288035, 0.4994605105243722, 0.22809297682791727], "isController": false}, {"data": ["POST Change Password", 10, 10, 100.0, 267.2, 253, 293, 260.5, 292.8, 293.0, 293.0, 0.9243852837862822, 0.6328067225919763, 0.2744268811240525], "isController": false}, {"data": ["GET Dashboard Direktur", 10, 10, 100.0, 267.1, 259, 294, 263.5, 291.8, 294.0, 294.0, 0.9257544899092761, 0.524353129050176, 0.17900330957230143], "isController": false}, {"data": ["PUT Update Employees", 10, 10, 100.0, 261.4, 248, 270, 262.5, 269.9, 270.0, 270.0, 0.9482268158543523, 0.5268955646690688, 0.514857528920918], "isController": false}, {"data": ["GET All Grades", 10, 0, 0.0, 259.1000000000001, 249, 272, 258.0, 271.8, 272.0, 272.0, 0.9270418095856123, 0.9532959233336423, 0.1557140539538333], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["400", 50, 55.55555555555556, 38.46153846153846], "isController": false}, {"data": ["401", 10, 11.11111111111111, 7.6923076923076925], "isController": false}, {"data": ["500", 30, 33.333333333333336, 23.076923076923077], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 130, 90, "400", 50, "500", 30, "401", 10, "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["DELETE Employees", 10, 10, "400", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["GET Dashboard", 10, 10, "400", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["GET All Grades by Level", 10, 10, "400", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": ["POST Create Employees", 10, 10, "500", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Forgot Password", 10, 10, "500", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Login", 10, 10, "401", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Change Password", 10, 10, "500", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET Dashboard Direktur", 10, 10, "400", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["PUT Update Employees", 10, 10, "400", 10, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
